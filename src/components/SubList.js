import React from 'react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Grid from '@material-ui/core/Grid';
import RequiredIcon from '@material-ui/icons/Lock';
import UnrequiredIcon from '@material-ui/icons/LockOpen';
import WatchIcon from '@material-ui/icons/WatchLaterOutlined';
import Timer from './Timer';

@inject('listStore')
@withRouter
@observer
class SubList extends React.Component {
  componentDidMount(){
    if (this.props.listStore.openedIndex==undefined) {this.props.listStore.defineOpened()}
  }
  getTabs = () => {
      const { listStore } = this.props;
      const list = listStore.subList.map((task,i) => {
          return (
              <ExpansionPanel
                  className={'ex-panel'+(task.status===1?' skipped':'')+(task.status===2?' done':'')+(i===listStore.openedIndex?' expanded':'')}
                  key={task.id}
                  expanded={i===listStore.openedIndex}
                  disabled = {i>listStore.firstUndoneRequired}
              >
                  <ExpansionPanelSummary
                       onClick={(e) => listStore.toggleTab(e,i)}
                  >
                      <Grid container spacing={0} className="exp-sum-grid">
                          <Grid item xs={6} className="taskTitle">
                              {task.title}
                          </Grid>
                          <Grid item xs={6} className="right">
                              {task.required? <RequiredIcon color="disabled"/> : <UnrequiredIcon color="disabled"/>}
                          </Grid>
                          
                          <Grid item xs={2} className="small">
                              Начало
                          </Grid>
                          <Grid item xs={8} className="center big">
                              {listStore.timeFormater(task.start)}<WatchIcon/>{listStore.timeFormater(task.end)}
                          </Grid>
                          <Grid item xs={2} className="right small">
                              Конец
                          </Grid>
                      </Grid>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                      <Grid container spacing={8}>
                              <Grid item xs={5} className="extra-big">
                                  {listStore.timeFormater(task.start)}
                              </Grid>
                              <Grid item xs={2} className="center">
                                  <WatchIcon/>
                              </Grid>
                              <Grid item xs={5} className="right extra-big">
                                  {listStore.timeFormater(task.end)}
                              </Grid>
                          <Grid item xs={12} className="timer-grid">
                              <Timer start={task.start} end={task.end}/>
                          </Grid>
                          <Grid item xs={12} className="big-but-grid">
                              <div>
                                  <div
                                      onClick={(e) => listStore.toggleStatus(e,i,2)}
                                      className={(task.required || task.status===1)?"big-but wide":"big-but"}
                                  >Готово</div>
                                  {(task.required || task.status===1)?  '': <div className="big-but" onClick={(e) => listStore.toggleStatus(e,i,1)}>Пропустить</div>}
                              </div>
                          </Grid>
                      </Grid>
                  </ExpansionPanelDetails>
              </ExpansionPanel>
          )
          
      })
      return list;
  }
  
  render() {
    const { listStore } = this.props;
    return (
        <div className="SubList">
        <div className="ocup">мастер</div>
        <h6 className="subname">{listStore.subName}</h6>
        {
            this.getTabs()
        }
        </div>
    );
  }
}

export default SubList;
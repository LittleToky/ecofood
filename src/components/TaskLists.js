import React from 'react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import RequiredIcon from '@material-ui/icons/Lock';
import UnrequiredIcon from '@material-ui/icons/LockOpen';
import EditIcon from '@material-ui/icons/CreateOutlined';
import ClearIcon from '@material-ui/icons/DeleteForever';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import AddIcon from '@material-ui/icons/Add';

@inject('listStore')
@withRouter
@observer

class TaskLists extends React.Component {
  render() {
    const { listStore } = this.props;
    return (
      <div className="TaskLists">
          <Paper className="papper">
            <h6>ДОБАВИТЬ СПИСОК ЗАДАЧ</h6>
            <form onSubmit={listStore.addList} id="newListForm">
            
                <div>
                
                    <div>
                        <TextField
                          label="Название списка"
                          margin="normal"
                          variant="outlined"
                          value = {listStore.newList.title}
                          fullWidth
                          onChange= {e => listStore.changeText(e,'newList','title')}
                        />
                    </div>
                    
                    <div className="select-grid">
                            
                        <FormControl variant="outlined" fullWidth>
                            <InputLabel
                                ref={ref => {
                                    this.InputLabelRef = ref;
                                }}
                            >
                                Подразделение
                            </InputLabel>
                            <Select
                                value={listStore.newList.executor}
                                input={
                                    <OutlinedInput
                                        labelWidth={120}
                                        name="Подразделение"
                                        fullWidth
                                    />
                                }
                                onChange={listStore.newListChangeExecutor}
                            >
                                <MenuItem value="">
                                    <em></em>
                                </MenuItem>
                                {
                                    listStore.executors.map(executor => {
                                        return (<MenuItem value={executor.id} key={executor.id}>{executor.title}</MenuItem>)
                                    })
                                }
                            </Select>
                        </FormControl>        
                    </div>
                    
                    <div>
                        <TextField
                         label="Описание"
                         multiline
                         margin="normal"
                         variant="outlined"
                         fullWidth
                         value={listStore.newList.description}
                         onChange= {e => listStore.changeText(e,'newList','description')}
                        />
                    </div>
                    
                    <div>
                        <Button variant="contained" color="primary" type="submit" className="button-mg-right">
                            Добавить
                        </Button>
                        
                        <Button variant="contained" color="default" onClick={listStore.resetNewList} type="reset">
                            Сброс
                        </Button>
                    </div>
                    
                </div>
            </form>
          </Paper>
          
           {
               listStore.lists.map(list => {
                   return (
                     <ExpansionPanel className="ex-panel" key={list.id}>
                         
                         <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                             <h6>{list.title}</h6>
                         </ExpansionPanelSummary>
                         
                         <ExpansionPanelDetails className="ex-p-d">
                         <div>
                         
                             <form onSubmit={(e) => listStore.saveList(e,list.id)} className="oldListForm">
                                 <div>
                                 
                                     <div>
                                         <TextField
                                           label="Название списка"
                                           margin="normal"
                                           variant="outlined"
                                           fullWidth
                                           defaultValue={list.title}
                                         />
                                     </div>
                                     
                                     <div className="select-grid">
                                             
                                         <FormControl variant="outlined" fullWidth>
                                             <InputLabel
                                                 ref={ref => {
                                                     this.InputLabelRef = ref;
                                                 }}
                                             >
                                                 Подразделение
                                             </InputLabel>
                                             <Select
                                                 value={list.executor}
                                                 input={
                                                     <OutlinedInput
                                                         labelWidth={120}
                                                         name="Подразделение"
                                                         fullWidth
                                                     />
                                                 }
                                                 onChange={e => listStore.listChangeExecutor(e, list.id)}
                                             >
                                                 <MenuItem value="">
                                                     <em></em>
                                                 </MenuItem>
                                                 {
                                                     listStore.executors.map(executor => {
                                                         return (<MenuItem value={executor.id} key={executor.id}>{executor.title}</MenuItem>)
                                                     })
                                                 }
                                             </Select>
                                         </FormControl>        
                                     </div>
                                 
                                     <div>
                                         <TextField
                                          label="Описание"
                                          multiline
                                          margin="normal"
                                          variant="outlined"
                                          fullWidth
                                          defaultValue={list.description}
                                         />
                                     </div>
                                     
                                     <div className="but-cont">
                                         <Button variant="contained" color="primary" type="submit"  className="button-mg-right">
                                             Сохранить
                                         </Button>
                                         
                                         <Button variant="contained" color="default" onClick={(e) => listStore.resetList(e,list.id)} type="reset">
                                             Сброс
                                         </Button>
                                     </div>
                                     
                                     <div className="del-but">
                                         <Button variant="contained" color="secondary" onClick={() => listStore.askDeleteList(list.id)}>
                                             Удалить Список
                                         </Button>
                                     </div>
                                </div> 
                             </form>
                             
                             <form className="newTaskForm" id="newTaskForm" onSubmit={(e) => listStore.addTask(e,list.id)}>
                                 <div>
                                      
                                      <div>
                                          <Divider className="divider"/>
                                          <h6>Задачи списка "{list.title}"</h6>
                                      </div>
                                      
                                      <div className="task-input-grid">
                                          <TextField
                                            label="Название задачи"
                                            margin="normal"
                                            variant="outlined"
                                            fullWidth
                                            value = {listStore.newTask.title}
                                            onChange= {e => listStore.changeText(e,'newTask','title')}
                                          />
                                      </div>
                                      
                                      <div className="time-cont">
                                          <TextField
                                            variant="outlined"
                                            label="начало"
                                            type="time"
                                            InputLabelProps={{
                                              shrink: true,
                                            }}
                                          />
                                      </div>
                                      
                                      <div className="time-cont right">
                                          <TextField
                                            variant="outlined"
                                            label="конец"
                                            type="time"
                                            InputLabelProps={{
                                              shrink: true,
                                            }}
                                          />
                                      </div>
                                      
                                      <div className="center">
                                          <FormControlLabel
                                              control={
                                                <Checkbox
                                                    checked={listStore.newTask.required}
                                                    onChange={listStore.newTaskChangeRequired}
                                                />
                                              }
                                              label="Обязательно для выполнения"
                                          />
                                      </div>
                                      
                                      <div>
                                          <Button variant="contained" color="primary" className="button-mg-right" type="submit">
                                              Добавить
                                          </Button>
                                          <Button variant="contained" color="default" onClick={listStore.resetNewTask} type="reset">
                                              Сброс
                                          </Button>
                                      </div>
                                 </div>
                             </form>
                             
                             <div>
                                     {
                                         list.tasks.map(task => {
                                             return (
                                                 
                                                 <ExpansionPanel className="outline-none-pannel" key={task.id}>
                                                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className="task-sum">
                                                        <div className="task-sum-wr">
                                
                                                             <div className="task-title-grid">
                                                                 {task.title}
                                                             </div>
                                                             
                                                             <div className="task-time-grid">
                                                                 <div>
                                                                     с {listStore.timeFormater(task.start)}
                                                                 </div>
                                                                 
                                                                 <div>
                                                                     до {listStore.timeFormater(task.end)}
                                                                 </div>
                                                             </div>
                                                         
                                                             <div className="task-require-grid">
                                                                {task.required? <RequiredIcon color="disabled"/> : <UnrequiredIcon color="disabled"/>}
                                                             </div>

                                                        </div>
                                                    </ExpansionPanelSummary>
                                                    
                                                    <ExpansionPanelDetails className="task-panel">
                                                        <form className="oldTaskForm" onSubmit={(e) => listStore.saveTask(e,list.id,task.id)}>    
                                                            <div>
                                                                <div className="task-input-grid">
                                                                    <TextField
                                                                      className="task-text"
                                                                      label="Название задачи"
                                                                      margin="normal"
                                                                      variant="outlined"
                                                                      defaultValue={task.title}
                                                                      fullWidth
                                                                    />
                                                                </div>
                                                                
                                                                <div className="time-cont">
                                                                    <TextField
                                                                      label="начало"
                                                                      defaultValue={listStore.timeFormater(task.start)}
                                                                      type="time"
                                                                      variant="outlined"
                                                                      InputLabelProps={{
                                                                        shrink: true,
                                                                      }}
                                                                    />
                                                                </div>
                                                                
                                                                <div className="time-cont right">
                                                                    <TextField
                                                                      label="конец"
                                                                      type="time"
                                                                      variant="outlined"
                                                                      defaultValue={listStore.timeFormater(task.end)}
                                                                      InputLabelProps={{
                                                                        shrink: true,
                                                                      }}
                                                                    />
                                                                </div>
                                                                
                                                                <div className="center">
                                                                    <FormControlLabel
                                                                        control={
                                                                          <Checkbox
                                                                              checked={task.tempRequired}
                                                                              onChange={(e) => listStore.changeTempRequired(e,list.id,task.id)}
                                                                          />
                                                                        }
                                                                        label="Обязательно для выполнения"
                                                                    />
                                                                </div>
                                                                
                                                                <div className="but-cont">
                                                                    <Button variant="contained" color="primary" type="submit"  className="button-mg-right">
                                                                        Сохранить
                                                                    </Button>
                                                                    
                                                                    <Button variant="contained" color="default" onClick={(e) => listStore.resetTask(e,list.id,task.id)}>
                                                                        Сброс
                                                                    </Button>
                                                                </div>
                                                                
                                                                <div className="del-but">
                                                                    <Button variant="contained" color="secondary" onClick={() => listStore.askDeleteTask(list.id,task.id)}>
                                                                        Удалить Задачу
                                                                    </Button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </ExpansionPanelDetails>
                                                 </ExpansionPanel>
                                                 
                                             )
                                         })
                                     }
                                   
                                 </div>
                            </div> 
                         </ExpansionPanelDetails>
                     </ExpansionPanel>
                   );
               })
           }
           
           <Paper className="papper">
               <h6 className="executor-heading">ПОДРАЗДЕЛЕНИЯ</h6>
               {
                   listStore.executors.map((executor,i) => {
                       return (
                           <div key={executor.id}>
                               <div className="executor-grid">
                                   <Input
                                   className = "exec-input"
                                   defaultValue={executor.title}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        <Button disabled className="pencil">
                                          <EditIcon/>
                                        </Button>
                                      </InputAdornment>
                                    }
                                  />
                                  <Button  onClick={() => listStore.askDeleteExecutor(i)}  variant="text" className="trash">
                                    <ClearIcon />
                                  </Button>
                               </div> 
                           </div> 
                       )
                   })
               }
               <div>
                   <form className="executor-grid" onSubmit={listStore.addExecutor}>
                       <Input
                       className = "exec-input"
                       placeholder="Добавить подразделеие"
                        endAdornment={
                          <InputAdornment position="end">
                            <Button disabled className="pencil">
                             <EditIcon/>
                            </Button>
                          </InputAdornment>
                        }
                      />
                      <Button type="submit" variant="text" className="trash">
                        <AddIcon />
                      </Button>
                   </form> 
               </div> 
           </Paper>
           
           <Dialog
              open={listStore.deleteExecutorDialog.open}
           >
              <DialogTitle >Удалить подразделение "{listStore.deleteExecutorDialog.title}"?</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Это действие будет нельзя отменить.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button  color="default" variant="outlined" onClick={listStore.closeDeleteExecutor}>
                  Отмена
                </Button>
                <Button  color="secondary" variant="contained" onClick={listStore.deleteExecutor}>
                  Удалить
                </Button>
              </DialogActions>
           </Dialog>
           
           <Dialog
              open={listStore.deleteListDialog.open}
           >
              <DialogTitle >Удалить список "{listStore.deleteListDialog.title}"?</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Вместе со списком будут удалены и содержащиеся в нем задачи! Это действие будет нельзя отменить.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button  color="default" variant="outlined" onClick={listStore.closeDeleteList}>
                  Отмена
                </Button>
                <Button  color="secondary" variant="contained" onClick={listStore.deleteList}>
                  Удалить
                </Button>
              </DialogActions>
           </Dialog>
           
           <Dialog
              open={listStore.deleteTaskDialog.open}
           >
              <DialogTitle >Удалить задачу "{listStore.deleteTaskDialog.title}"?</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Это действие будет нельзя отменить.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button  color="default" variant="outlined" onClick={listStore.closeDeleteTask}>
                  Отмена
                </Button>
                <Button  color="secondary" variant="contained" onClick={listStore.deleteTask}>
                  Удалить
                </Button>
              </DialogActions>
           </Dialog>
      </div>
    );
  }
}

export default TaskLists;
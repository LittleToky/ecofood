import React from 'react';

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            perc: '0%'
        }
    }
    componentDidMount(){
        this.recount(this);
        setInterval(this.recount,60000,this);
    }
    recount = (self) => {
        const full = self.props.end - self.props.start;
        let now = new Date();
        now = now.getHours()*60 + now.getMinutes();
        if (now < self.props.start) {
            self.setState({'perc':'0%'});
        } else if (now > self.props.end) {
            self.setState({'perc':'100%'});
        } else {
            self.setState({'perc':(now - self.props.start)*100/full+'%'});
        }
    }
    render() {
        return (
            <div className="timer-full">
                <div className="timer-green"></div>
                <div className="timer-yellow"></div>
                <div className="timer-arrow" style={{'left': this.state.perc}}></div>
            </div>
        );
    }
}
export default Timer;
import { observable, action } from 'mobx';
import agent from '../agent';

class ListStore {
    
  // FOR Sub
  
  @observable subName = "Иванов Иван Иванович"
  
  @observable subList = [
      {
          'title' : 'Задание 1',
          'id' : '1',
          'required' : true,
          'start' : 600,
          'end' : 660,
          'status' : 2
      },{
          'title' : 'Задание 2',
          'id' : '2',
          'required' : false,
          'start' : 660,
          'end' : 720,
          'status' : 1
      },{
          'title' : 'Задание 3',
          'id' : '3',
          'required' : true,
          'tempRequired' : true,
          'start' : 720,
          'end' : 780,
          'status' : 2
      },{
          'title' : 'Задание 4',
          'id' : '4',
          'required' : false,
          'tempRequired' : false,
          'start' : 780,
          'end' : 840,
          'status' : 0
      },{
          'title' : 'Задание 5',
          'id' : '5',
          'required' : true,
          'tempRequired' : false,
          'start' : 840,
          'end' : 900,
          'status' : 0
      },{
          'title' : 'Задание 6',
          'id' : '6',
          'required' : false,
          'tempRequired' : false,
          'start' : 900,
          'end' : 960,
          'status' : 0
      },{
          'title' : 'Задание 7',
          'id' : '7',
          'required' : false,
          'tempRequired' : false,
          'start' : 960,
          'end' : 1020,
          'status' : 0
      }
  ]
  
  @observable openedIndex = undefined;
  
  @observable firstUndoneRequired = undefined;
  
  @action defineOpened = () => {
      const firstUndone = this.subList.filter(task => task.status===0)[0];
      let firstUndoneIndex = this.subList.indexOf(firstUndone);
      firstUndoneIndex = firstUndoneIndex<0 ? this.subList.length : firstUndoneIndex;
      this.openedIndex = firstUndoneIndex;
      const firstUndoneRequired = this.subList.filter(task => (task.status===0 && task.required))[0];
      let firstUndoneRequiredIndex = this.subList.indexOf(firstUndoneRequired);
      firstUndoneRequiredIndex = firstUndoneRequiredIndex<0 ? this.subList.length : firstUndoneRequiredIndex;
      this.firstUndoneRequired = firstUndoneRequiredIndex;
  }
  
  @action toggleTab = (e,i) => {
      if (this.subList[i].status!==2 && i<=this.firstUndoneRequired) {
          this.openedIndex = i;
      }
  }
  
  @action toggleStatus = (e,i,status) => {
      this.subList[i].status = status;
      this.defineOpened();
  }
  
  // FOR MANAGER    
  // data variables
  
  @observable lists = [
      {
          'title' : 'Список 1',
          'id' : '1',
          'executor' : '1',
          'description' : 'description of Список 1',
          'tasks' : [
              {
                  'title' : 'Задание 1',
                  'id' : '1',
                  'required' : true,
                  'tempRequired' : true,
                  'start' : 600,
                  'end' : 660
              },{
                  'title' : 'Задание 2',
                  'id' : '2',
                  'required' : false,
                  'tempRequired' : false,
                  'start' : 660,
                  'end' : 720
              }
          ]
      },{
          'title' : 'Список 2',
          'id' : '2',
          'executor' : '2',
          'description' : 'description of Список 2',
          'tasks' : [
              {
                  'title' : 'Задание 1',
                  'id' : '3',
                  'required' : true,
                  'tempRequired' : true,
                  'start' : 600,
                  'end' : 660
              },{
                  'title' : 'Задание 2',
                  'id' : '4',
                  'required' : false,
                  'tempRequired' : false,
                  'start' : 660,
                  'end' : 720
              }
          ]
      }
  ]
  
  @observable executors = [
      {
          'id' : '1',
          'title' : 'Цех сухих смесей'
      },{
          'id' : '2',
          'title' : 'Горячий цех'
      }
  ]
  
  
  
  // new data variables
  
  @observable newList={
      'title' : '',
      'id' : 'newId',
      'executor' : '',
      'description' : ''
  }
  
  @observable newTask={
      'title' : '',
      'required' : ''
  }
  
  
  
  // view variables
  
  @observable deleteListDialog = {
      open: false
  }
  
  @observable deleteTaskDialog = {
      open: false
  }
  
  @observable deleteExecutorDialog = {
      open: false
  }
  
  
  
  // lists functions
  
  @action addList = e => { // creates new list
      e.preventDefault();
      const tempList = {
          'title' : this.newList.title,
          'executor' : this.newList.executor,
          'description' : this.newList.description,
          'id': (new Date()).getTime(), // нужно в случае отсутствия соединения, иначе заменить ответом сервера
          'tasks': []
      }
      //отправить на сервер, получить id, подставить в tempList
      this.lists.push(tempList);
      this.newList.executor = '';
      this.newList.title = '';
      this.newList.description = '';
  } 
  
  @action deleteList = () => {
      this.lists = this.lists.filter(list => list.id !== this.deleteListDialog.id);
      this.closeDeleteList();
  }
  
  @action saveList = (e,id) => { // saves changes in existing list
      e.preventDefault();
      const result = this.lists.filter(list => list.id === id)[0];
      const elems = e.target.elements;
      result.title = elems[1].value;
      delete(result.initialExecutor);
      result.description = elems[6].value;
      //отправить на сервер
  }
  
  @action listChangeExecutor = (e,id) => {
      const result = this.lists.filter(list => list.id === id)[0];
      result.initialExecutor = result.executor;
      result.executor = e.target.value;
  }
  
  @action newListChangeExecutor = e => {
      this.newList.executor = e.target.value;
  }
  
  @action resetList = (e,id) => {
      const result = this.lists.filter(list => list.id === id)[0];
      result.executor = result.initialExecutor ? result.initialExecutor : result.executor;
      delete(result.initialExecutor);
  }
  
  @action resetNewList = () => {
      this.newList.executor = '';
      this.newList.title = '';
      this.newList.description = '';
  }
  
  @action askDeleteList = id => { // set parsms for list deletation, opens dialog
      const result = this.lists.filter(list => list.id === id)[0];
      this.deleteListDialog.id = id;
      this.deleteListDialog.title = result.title;
      this.deleteListDialog.open = true;
  }
  
  @action changeText = (e,obj,prop) => { // also use for tasks
      this[obj][prop] = e.target.value;
  }
  
  
  
  // task functions
 
  @action addTask = (e,id) => { // id of list to insert new task in
      e.preventDefault();
      const elems = e.target.elements;
      const result = this.lists.filter(list => list.id === id)[0];
      const tempTask = {
          'title' : this.newTask.title,
          'id': (new Date()).getTime(), // нужно в случае отсутствия соединения, иначе заменить ответом сервера
          'start': this.timeFormaterReverse(elems[3].value),
          'end': this.timeFormaterReverse(elems[5].value),
          'required': this.newTask.required,
          'tempRequired': this.newTask.required
      }
      //отправить на сервер, получить id, подставить в tempTask
      result.tasks.push(tempTask);
      this.resetNewTask(e);
      e.target.reset();
  }
  
  @action resetNewTask = e => {
      this.newTask.title = '';
      this.newTask.required = false;
      this.newTask.tempRequired = false;
  }
  
  @action newTaskChangeRequired = e => { // need to reset required checkbox
      this.newTask.required = e.target.checked;
  }
  
  @action changeTempRequired = (e,listID,taskID) => {
      this.lists.filter(list => list.id === listID)[0].tasks.filter(task => task.id === taskID)[0].tempRequired=e.target.checked;
  }
  
  @action saveTask = (e,listID,taskID) => {
      e.preventDefault();
      const result = this.lists.filter(list => list.id === listID)[0].tasks.filter(task => task.id === taskID)[0];
      const elems = e.target.elements;
      result.required = result.tempRequired;
      result.title=elems[1].value;
      const t1 = this.timeFormaterReverse(elems[3].value);
      const t2 = this.timeFormaterReverse(elems[5].value);
      result.start = (t1>t2)?t2:t1;
      result.end = (t1>t2)?t1:t2;
      elems[3].value = this.timeFormater(result.start);
      elems[5].value = this.timeFormater(result.end);
      //отправить на сервер
  }
  
  @action resetTask = (e,listID,taskID) => {
      const result = this.lists.filter(list => list.id === listID)[0].tasks.filter(task => task.id === taskID)[0];
      const elems = e.target.closest('form').elements;
      elems[1].value = result.title;
      result.tempRequired = result.required;
      elems[3].value = this.timeFormater(result.start);
      elems[5].value = this.timeFormater(result.end);
  }
  
  @action askDeleteTask = (listID,taskID) => {
      const result = this.lists.filter(list => list.id === listID)[0].tasks.filter(task => task.id === taskID)[0];
      this.deleteTaskDialog.listID = listID;
      this.deleteTaskDialog.taskID = taskID;
      this.deleteTaskDialog.title = result.title;
      this.deleteTaskDialog.open = true;
  }
  
  @action deleteTask = (e) => {
      const list = this.lists.filter(list => list.id === this.deleteTaskDialog.listID)[0];
      list.tasks = list.tasks.filter(task => task.id !== this.deleteTaskDialog.taskID);
      this.closeDeleteTask();
  }
  
  
  
  //executor functions
  
  @action addExecutor = (e) => {
      e.preventDefault();
      this.executors.push({
          'id' : Math.random(),
          'title' : e.target.elements[0].value
      });
      e.target.elements[0].value='';1
  }
  
  @action askDeleteExecutor(i) {
      const result = this.executors[i];
      this.deleteExecutorDialog.id = result.id;
      this.deleteExecutorDialog.title = result.title;
      this.deleteExecutorDialog.open = true;
      debugger
  }
  
  @action deleteExecutor = () => {
      this.executors = this.executors.filter(executor => executor.id !== this.deleteExecutorDialog.id);
      this.closeDeleteExecutor();
  }
  
  
  // view functions
  
  @action closeDeleteList = () => {
      this.deleteListDialog.open = false;
      delete(this.deleteListDialog.id);
  }
  
  @action closeDeleteTask = () => {
      this.deleteTaskDialog.open = false;
      delete(this.deleteTaskDialog.taskID);
      delete(this.deleteTaskDialog.listID);
  }
  
  @action closeDeleteExecutor = () => {
      this.deleteExecutorDialog.open = false;
      delete(this.deleteExecutorDialog.executorID);
  }
  
  
  
  // other functions
  
  timeFormater = time => {
        var h = String(Math.floor(time/60));
        var m = String(time%60);
        h = h.length>1?h:'0'+h;
        m = m.length>1?m:'0'+m;
        if (h==="NaN" || m==="NaN") {
            return '__:__'
        }
        return h+':'+m
    }
  
  timeFormaterReverse = time => { // from hh:mm to m
      const t = time.split(':');
      return t[0]*60+t[1]*1
  }
  
}

export default new ListStore();
